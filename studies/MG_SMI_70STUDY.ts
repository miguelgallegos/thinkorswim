declare lower;
input kLen = 70;
input dLen = 10;
def st1 = StochasticMomentumIndex("percent k length" = kLen, "percent d length" = dLen);
def st2 = StochasticMomentumIndex("percent k length" = kLen, "percent d length" = dLen).AvgSMI;

#  OS = -40 | OB = 40 | mid = 0

def mcdVal = MACD();
def mcdAvg = MACD().Avg;

def trigger = -40;

#plot sig1 = st1 > trigger
#AND mcdVal > mcdAvg
#and st1 > st1[1] 
#AND st1 > st2
#; 

plot s1 = st1 > st2 AND st1 > -40
OR (st1 > 40 AND st2 > 40)
;