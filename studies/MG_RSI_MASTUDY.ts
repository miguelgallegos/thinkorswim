declare lower;
input obvLength = 50;
def o1 = RSI();

plot oma = MovingAverage(data = o1, length = obvLength, averageType = AverageType.WEIGHTED);

#plot sig1 = o1 < o1[1] ;#* 3

oma.DefineColor("DOWN", GetColor(4));
oma.DefineColor("Normal", GetColor(7));
oma.DefineColor("UP", GetColor(6));
oma.AssignValueColor(if oma > oma[1] then oma.color("UP") else if oma < oma[1] then oma.color("DOWN") else oma.color("Normal"));