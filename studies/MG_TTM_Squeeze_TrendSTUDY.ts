declare lower;
input price = CLOSE;
input length = 20;
input nK = 1.5;
input nBB = 2.0;
input alertLine = 1.0;
input lengthMomentum = 12;
def sqzHist = TTM_Squeeze(price, length, nK, nBB, alertLine);

input compBars = 6;
input paintBars = yes;
def upTrend = TTM_Trend(compBars, paintBars).TrendUp;

plot mom = Momentum();#price - price[lengthMomentum] ;
def sent = MarketSentiment();
#MG Signal
plot trendingUp = upTrend AND sqzHist > 0 AND mom > 0
    #AND sent > sent[1]
;
AssignPriceColor(if trendingUp then Color.GREEN else Color.GRAY); #.CURRENT