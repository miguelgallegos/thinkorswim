declare lower;
input price = FundamentalType.LOW;
input smaLength = 50;
input trendLength = 11;

def priceVix = Fundamental(price, "VIX");
def smaVix = Average(priceVix, smaLength);


#AddOrder(OrderType.BUY_AUTO,
#    Sum(priceVix < smaVix, trendLength)[1] == trendLength,
#    tickcolor = GetColor(1),
#    arrowcolor = GetColor(1),
#    name = "VIX_Timing_LE");

#plot val1 = Sum(priceVix < smaVix, trendLength);

plot buy = Sum(priceVix < smaVix, trendLength)[1] == trendLength;

plot sell = Sum(priceVix > smaVix, trendLength)[1] == trendLength; 