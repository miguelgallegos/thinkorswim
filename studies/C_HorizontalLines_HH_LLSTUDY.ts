#Hint: Plots Horizontal lines of highest-highs and lowest-lows
#TOS Name = HorizLines_HH_LL
input length = 20; #hint length: The number of bars being evaluated for the HH and LL. Also is the length of the longest lines. Longer lines may show when two adjacent lines have the same HH or LL values.

input ShowHi_Lines = yes;#hint ShowHi_Lines:Shows or hides HH lines
input ShowLo_Lines = yes;#hint ShowLo_Lines:Shows or hides LL lines

def a = Lowest(low, length);
def b = if low == a then a else b[1];

plot LL_Lines = if b == 0 then double.nan else if ShowLo_Lines then b else double.nan;
LL_Lines.SetPaintingStrategy(paintingStrategy.hORIZONTAL);

def d = Highest(high, length);
def e = if high >= d then d else e[1];

plot HH_Lines = if ShowHi_Lines && e == 0 then double.nan else if ShowHi_Lines then e else double.nan;
HH_Lines.SetPaintingStrategy(paintingStrategy.horizontal);