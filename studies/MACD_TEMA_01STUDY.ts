#
# TD Ameritrade IP Company, Inc. (c) 2009-2018
#
declare lower;
input price = close;
input length = 10;

def ema1 = ExpAverage(price, length);
def ema2 = ExpAverage(ema1, length);
def ema3 = ExpAverage(ema2, length);

def TEMA = 3 * ema1 - 3 * ema2 + ema3;
#plot
def temaPlot = price > TEMA and TEMA[1] < TEMA;
#temaPlot.SetDefaultColor(GetColor(0));

#MACD
input fastLength = 12;
input slowLength = 26;
input MACDLength = 9;
input averageType = AverageType.EXPONENTIAL;
input showBreakoutSignals = no;

input lenghtSmaTrend = 200;
#custom
def Value = MovingAverage(averageType, close, fastLength) - MovingAverage(averageType, close, slowLength);
def Avg = MovingAverage(averageType, Value, MACDLength);
def Diff = Value - Avg;

def theDiff = Diff >= 0.0;

def smaTrend = MovingAverage(AverageType.SIMPLE, close, lenghtSmaTrend);


#stoch mom idx
input smi_over_bought = 40.0;
#input smi_over_sold = -40.0;
input percentDLength = 3;
input percentKLength = 5;

def min_low = lowest(low, percentKLength);
def max_high = highest(high, percentKLength);
def rel_diff = close - (max_high + min_low)/2;
def smi_diff = max_high - min_low;

def avgrel = expaverage(expaverage(rel_diff, percentDLength), percentDLength);
def avgdiff = expaverage(expaverage(smi_diff, percentDLength), percentDLength);

#plot SMI = if avgdiff != 0 then avgrel / (avgdiff / 2) * 100 else 0;
def SMI = if avgdiff != 0 then avgrel / (avgdiff / 2) * 100 else 0;

plot myDiff = (
SMI[1] < SMI and
SMI[2] < SMI[1] and
SMI < smi_over_bought and 
theDiff and 
temaPlot 
and price > smaTrend 
and smaTrend[1] < smaTrend);