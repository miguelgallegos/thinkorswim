declare lower;
input maLen = 50;
def smi = StochasticMomentumIndex("percent k length" = 70);
def smiAvg = StochasticMomentumIndex("percent k length" = 70).AvgSMI;

def ma = MovingAverage(data = CLOSE, length = maLen);

plot s1 = CLOSE > ma 
AND (
    #smi crosses above smiAvg 
    #OR smi[1] crosses above smiAvg[1]
    smi > smiAvg 
)
;
