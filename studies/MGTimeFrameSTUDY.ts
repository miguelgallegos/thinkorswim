#
# MG
#
def ms = MarketSentiment();
declare lower;

input sma1 = 50;
def dailyClose = close;
def weeklyOpen = open(period = AggregationPeriod.WEEK);
def weeklyClose = close(period = AggregationPeriod.WEEK);

def monthlyOpen = open(period = AggregationPeriod.MONTH);
def monthlyClose = close(period = AggregationPeriod.MONTH);

def SMA = Average(dailyClose, sma1);

def signal1 = 1 or  ### TEST
    ((monthlyClose > monthlyClose[1] 
        and monthlyClose[1] > monthlyOpen[1]) 
and 
    (weeklyClose > weeklyClose[1] 
        and weeklyClose[1] > weeklyOpen[1]))
#and (dailyClose > SMA)
;
#plot ms1 = ms;
#plot ms2 = ms[3];
def signal2 = dailyClose > SMA;

#plot periodsCheck = signal1;
#plot aboveSma = signal2;
plot signal3 = signal1 and signal2 
 AND ms > ms[3]
;

#signal1.SetDefaultColor(Color.DARK_GREEN);
#signal2.SetDefaultColor(Color.DARK_ORANGE);
#signal3.SetDefaultColor(Color.ORANGE);
#plot mc1 = monthlyClose;

