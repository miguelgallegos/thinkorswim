#
# TD Ameritrade IP Company, Inc. (c) 2008-2019
#

declare lower;

input over_bought = 40.0;
input over_sold = -40.0;
input percentDLength = 3;
input percentKLength = 5;

def min_low = lowest(low, percentKLength);
def max_high = highest(high, percentKLength);
def rel_diff = close - (max_high + min_low)/2;
def diff = max_high - min_low;

def avgrel = expaverage(expaverage(rel_diff, percentDLength), percentDLength);
def avgdiff = expaverage(expaverage(diff, percentDLength), percentDLength);

def SMI = if avgdiff != 0 then avgrel / (avgdiff / 2) * 100 else 0;
#smi.setDefaultColor(getColor(1));

def AvgSMI = expaverage(smi, percentDLength);
#avgsmi.setDefaultColor(getcolor(5));

#plot overbought = over_bought;
#overbought.setDefaultColor(getcolor(5));
def oversold = over_sold;
#oversold.setDefaultColor(getcolor(5));

## EMA

input price = close;
input lengthFast = 8;
input displaceFast = 0;
input showBreakoutSignalsFast = no;

input lengthSlow = 50;
input displaceSlow = 0;

def AvgExpFast = ExpAverage(price[-displaceFast], lengthFast);

def AvgExpSlow = ExpAverage(price[-displaceSlow], lengthSlow);

plot smiUp = (SMI crosses above over_sold OR SMI crosses above AvgSMI OR SMI > AvgSMI) AND AvgExpFast > AvgExpSlow AND price > AvgExpSlow;
#smiUp.SetPaintingStrategy(PaintingStrategy.ARROW_UP);

