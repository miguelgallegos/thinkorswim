#plot Data = close;
declare lower;
input len = 50;
def PETD = TEMA(close, len);

plot trend = close > PETD;

AssignPriceColor(if close > PETD then Color.GREEN else Color.RED);