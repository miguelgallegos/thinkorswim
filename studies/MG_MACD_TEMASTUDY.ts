declare lower;

def t1 = TEMA(length = 50);
def macdV = MACD();
def macdA = MACD().Avg;

def r1 = RSI();

plot b = 
close > t1 
AND t1 > t1[1]
AND t1[1] > t1[2]
#AND r1 > 50
AND macdV > macdA
;