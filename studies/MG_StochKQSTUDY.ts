declare lower;

def r1 = RSI();
def s1 = StochasticMomentumIndex("percent k length" = 14);
def s1Avg = StochasticMomentumIndex("percent k length" = 14).AvgSMI;

def s2 = StochasticMomentumIndex();
def s2Avg = StochasticMomentumIndex().AvgSMI;

def m1Val = MACD().Value;
def m2Avg = MACD().Avg;
def ma1 = MovingAverage(length = 20, data = CLOSE);
def ma2 = MovingAverage(length = 50, data = CLOSE);

def longStoch = StochasticSlow("k period" = 70);

def buy = 
(
#r1 > 50
#AND s1 > 0
###AND s1 >= -15
#AND m1Val > m2Avg


###AND ma2 > ma2[1]
#AND ma1 > ma2
#AND 
close > ma2
)
#OR longStoch > 80
;

plot entry = ( s2 > s2Avg ) ;

entry.SetPaintingStrategy(paintingStrategy = PaintingStrategy.LINE_VS_TRIANGLES);

entry.AssignValueColor(
 if buy and entry then Color.MAGENTA else Color.GRAY
);

AssignPriceColor(if entry then Color.GREEN else Color.GRAY);