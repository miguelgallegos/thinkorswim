#study(title="TTM scalper indicator", overlay = true)
#width = input(2, minval=1)
declare lower;
###input width = 2;
###input price = CLOSE;

###plot triggerSell =  price[1] < price AND (price[2] < price[1] OR price[3] < price[1]);

###plot triggerBuy =  price[1] > price AND (price[2] > price[1] OR price[3] > price[1]);

plot hi = Highest(high,3);
plot lo = Lowest(low, 3);


#triggerSell = iff(iff(close[1] < close,1,0) and (close[2] < close[1] or close[3] <close[1]),1,0)
#triggerBuy = iff(iff(close[1] > close,1,0) and (close[2] > close[1] or close[3] > close[1]),1,0)
#buySellSwitch = iff(triggerSell, 1, iff(triggerBuy, 0, nz(buySellSwitch[1])))
#SBS = iff(triggerSell and buySellSwitch[1] == false, high, iff(triggerBuy and buySellSwitch[1], low, nz(SBS[1])))
#plot(SBS, color=blue, title="TTM", style = circles, linewidth = width)