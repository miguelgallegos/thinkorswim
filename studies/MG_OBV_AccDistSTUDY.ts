declare lower;

input maLen = 50;
def o1 = OnBalanceVolume();
def a1 = AccDist();

def aoAvg = (o1 + a1) / 2;

plot movAvg = MovingAverage(data = aoAvg, length = maLen, averageType = AverageType.EXPONENTIAL);

plot zero = 0;

movAvg.DefineColor("DOWN", GetColor(4));
movAvg.DefineColor("Normal", GetColor(7));
movAvg.DefineColor("UP", GetColor(6));
movAvg.AssignValueColor(if movAvg > movAvg[1] then movAvg.Color("UP") else if movAvg < movAvg[1] then movAvg.Color("DOWN") else movAvg.Color("Normal"));