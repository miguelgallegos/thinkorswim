declare lower;

def zp = ZigZagTrendPercent();
#def zs = ZigZagTrendSign();

plot signalZPMain = zp == 1;

AssignPriceColor(if zp == 1 then Color.CYAN else Color.ORANGE);