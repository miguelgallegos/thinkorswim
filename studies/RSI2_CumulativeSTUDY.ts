#RSI
declare lower;

input length = 2;
input over_Bought = 90;
input over_Sold = 10;
input price = close;
input averageType = AverageType.WILDERS;
#input showBreakoutSignals = no;

def NetChgAvg = MovingAverage(averageType, price - price[1], length);
def TotChgAvg = MovingAverage(averageType, AbsValue(price - price[1]), length);
def ChgRatio = if TotChgAvg != 0 then NetChgAvg / TotChgAvg else 0;

def RSI = 50 * (ChgRatio + 1);


# SMA
#input price = close;
input lengthSimpleMovingAverage = 200;
input displace = 0;
input cumulativeRSI = 35;
input cumulativeRSIDays = 2;
#input showBreakoutSignals = no;

def SMA = Average(price[-displace], lengthSimpleMovingAverage);

plot RSI2_Signal = (price > SMA and sum(RSI, cumulativeRSIDays) <= cumulativeRSI) 
#AND price < SMA * 1.05
;
#* -1 + (RSI[1] >= over_Bought and RSI <= over_Bought) * 1

