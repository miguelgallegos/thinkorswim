declare lower;

input obvMALen = 14;
input maLen = 15;

def bbUpper = BollingerBands().UpperBand;

def obv = OnBalanceVolume();

def obvMA = MovingAverage(length = obvMALen, data = obv);

def ema1 = MovingAverage(length = maLen, data = CLOSE, averageType = AverageType.EXPONENTIAL);

def r = RSI();

def smi = StochasticMomentumIndex();

plot signal = 
close > ema1 
AND r > 50
#AND smi > 0
AND obvMA > obvMA[1]

#test pure obv:
###AND obv > obv[1] # gives on/off a lot
;
AssignPriceColor(if signal then Color.GREEN else Color.RED);


