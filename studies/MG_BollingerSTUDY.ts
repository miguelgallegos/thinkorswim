#MG - Steven Primo
#declare lower;
input price = close;
input high = high;
input displace = 0;
input length = 20;
input lengthTrend = 50;
input Num_Dev_Dn = -0.382;
input Num_Dev_up = 0.382;
input averageType = AverageType.Simple;

def sDev = stdev(data = price[-displace], length = length);

plot MidLine = MovingAverage(averageType, data = price[-displace], length = length);
def smaTrend = Average(price[-displace], lengthTrend);

def LowerBand = MidLine + num_Dev_Dn * sDev;
def UpperBand = MidLine + num_Dev_Up * sDev;

#ENTRY -- any of the below ones
def b1 = 
    price > UpperBand
    AND (high > price[1] OR price > price[1])
    AND price[1] > UpperBand[1]
    AND price[2] > UpperBand[2]
    AND price[3] > UpperBand[3]
    AND price[4] > UpperBand[4]
    AND price[5] > UpperBand[5]
    #trend
    #AND smaTrend > (smaTrend[1] * 1.00)
    #### trend check-> AND price >= smaTrend
;

###smaTrend.SetDefaultColor(GetColor(10));
#Draw arrows
#EXIT
#plot UpSignal = high crosses above price[1] AND price > smaTrend;
#plot UpSignal = b1 crosses above 0
#;

#AssignPriceColor(if price > smaTrend then Color.GREEN else Color.GRAY);
AssignPriceColor(if b1 then Color.GREEN else Color.GRAY);
plot PrimoBBSignal = b1;
#UpSignal.SetDefaultColor(Color.MAGENTA);
#UpSignal.SetPaintingStrategy(PaintingStrategy.BOOLEAN_ARROW_UP);


#LowerBand.SetDefaultColor(GetColor(0));
#MidLine.SetDefaultColor(GetColor(1));
#UpperBand.SetDefaultColor(GetColor(5));