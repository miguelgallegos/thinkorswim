declare lower;

def r = RSI();

plot rsiSig = if r crosses above 30 then -1 else if r crosses below 70 then 1 else 0;