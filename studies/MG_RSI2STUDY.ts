declare lower;
input maTrendLen = 50;
input maFollowLen = 5;
def r1 = RSI(length = 2);
def ma = MovingAverage(data = CLOSE, length = maTrendLen);
def maFollow = MovingAverage(data = CLOSE, length = maFollowLen, averageType = AverageType.EXPONENTIAL);

def addUp = r1 + r1[1];

plot sigIn = 
#r1 > r1[1] 
#AND 
#maFollow > maFollow[1]
#AND ma > ma[1]
#AND CLOSE > ma
#maFollow > maFollow[1]
#AND CLOSE < maFollow
    r1 < 10 AND
    ma > ma[1] 
AND CLOSE > ma
;
#plot sig1 = addUp < 15
#AND CLOSE > ma
#;