declare lower;

input periodBackStdDevHigh = 22;# LookBack Period Standard Deviation High
input bbLen = 20; # Bolinger Band Length
input bbStdDevUp = 2.0; # Bollinger Band Standard Deviation Up
input lookBackPercentilHigh = 50; # Look Back Period Percentile High
input highestPercentile = 0.85; # Highest Percentile - 0.90=90%, 0.95=95%, 0.99=99%

input lowestPercentile = 1.01; # Lowest Percentile - 1.10=90%, 1.05=95%, 1.01=99%

input showHighRange = 0; # Show High Range - Based on Percentile and LookBack Period?

input showStdDevLine = 0; # Show Standard Deviation Line?

input averageType = AverageType.Simple;

#plot tst1 = Highest(close, periodBackStdDevHigh) - low;
#plot tst2 = Highest(close, periodBackStdDevHigh);
plot wvf = ((Highest(close, periodBackStdDevHigh) - low) /
    (Highest(close, periodBackStdDevHigh))) * 100 ;

def sDev = bbStdDevUp * stdev(wvf, bbLen);

def midLine = MovingAverage(averageType, wvf, bbLen);

def lowerBand = midLine - sDev;
def upperBand = midLine + sDev;

plot rangeHigh = (Highest(wvf, lookBackPercentilHigh)) * highestPercentile;
def rangeLow = (Lowest(wvf, lookBackPercentilHigh)) * lowestPercentile;

#def col = if wvf >= upperBand or wvf >= rangeHigh then Color.GREEN else Color.GRAY;

#plot ts1 = highestPercentile;

#plot t2 = highestPercentile and rangeLow;

#plot(hp and rangeHigh ? rangeHigh : na, title="Range High Percentile", style=line, linewidth=4, color=orange)
#plot(hp and rangeLow ? rangeLow : na, title="Range High Percentile", style=line, linewidth=4, color=orange)
#plot(wvf, title="Williams Vix Fix", style=histogram, linewidth = 4, color=col)
wvf.SetPaintingStrategy(PaintingStrategy.HISTOGRAM);
wvf.AssignValueColor(if wvf >= upperBand or wvf >= rangeHigh then Color.GREEN else Color.GRAY);


#plot(sd and upperBand ? upperBand : na, title="Upper Band", style=line, linewidth = 3, color=aqua)



