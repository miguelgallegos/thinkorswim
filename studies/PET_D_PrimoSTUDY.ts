#plot Data = close;

def PETD = ExpAverage(close, 15);

#plot up = close > petd ? green : red;
#barcolor(up)

#plot OverBought = 80;
#plot OverSold = 20;

#PETD.DefineColor("UP", Color.ORANGE);
#PETD.DefineColor("DOWN", Color.BLUE);

#OverBought.SetDefaultColor(Color.GRAY);
#OverSold.SetDefaultColor(Color.GRAY);

AssignPriceColor(if close > PETD then Color.GREEN else Color.RED);