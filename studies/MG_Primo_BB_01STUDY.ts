#MG - Steven Primo
#declare lower;
input price = close;
input high = high;
input displace = 0;
input length = 20;
input lengthTrend = 50;
input Num_Dev_Dn = -.382;
input Num_Dev_up = .382;
input averageType = AverageType.SIMPLE;

def sDev = StDev(data = price[-displace], length = length);

def MidLine = MovingAverage(averageType, data = price[-displace], length = length);
def smaTrend = Average(price[-displace], lengthTrend);

def LowerBand = MidLine + Num_Dev_Dn * sDev;
plot UpperBand = MidLine + Num_Dev_up * sDev;

#ENTRY -- any of the below ones

def b1 = 
#base rules
    price[0] >= smaTrend[0] AND
    price[1] >= smaTrend[1] AND
    price[2] >= smaTrend[2] 
 
    and price[1] > UpperBand[1]
    and price[2] > UpperBand[2]
    and price[3] > UpperBand[3]
    and price[4] > UpperBand[4]
    and price[5] > UpperBand[5]
    #trend
    #AND smaTrend > (smaTrend[1] * 1.00)
    
#signal
    AND price[0] >= highest(price[0],6)

;

plot primoBuy = b1;

#plot h = highest(price[0],6);

primoBuy.SetDefaultColor(Color.MAGENTA);
primoBuy.SetPaintingStrategy(PaintingStrategy.BOOLEAN_ARROW_UP);

#smaTrend.SetDefaultColor(GetColor(10));
#Draw arrows
#EXIT
#plot UpSignal = high crosses above price[1] AND price > smaTrend;
#plot UpSignal = b1 crosses above 0;

#AssignPriceColor(if price > smaTrend then Color.GREEN else Color.GRAY);
#UpSignal.SetDefaultColor(Color.MAGENTA);
#UpSignal.SetPaintingStrategy(PaintingStrategy.BOOLEAN_ARROW_UP);


#LowerBand.SetDefaultColor(GetColor(0));
#MidLine.SetDefaultColor(GetColor(1));
#UpperBand.SetDefaultColor(GetColor(5));