declare lower;
input length = 14;
input averageType = AverageType.WILDERS;

def var1 = ADX(length, averageType).ADX > 30 and
DMI(length, averageType)."DI+" > DMI(length, averageType)."DI-" and
((low < low[1] and low[1] < low[2] and low[2] < low[3]) or
((low < low[1] and low[1] < low[2] and high[2] < high[3] and low[2] > low[3]) or
(low < low[1] and low[2] < low[3] and high[1] < high[2] and low[1] > low[2]) or
(low[1] < low[2] and low[2] < low[3] and high < high[1] and low > low[1])));

plot scan = var1 and close > Average(close, 50) and close > Average(close, 200);