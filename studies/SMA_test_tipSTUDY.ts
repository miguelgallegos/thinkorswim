declare lower;

input price = close;
input length = 200;
input displace = 0;
input showBreakoutSignals = no;

def SMA = Average(price[-displace], length);

#RSI
input rsi_length = 5;
input over_Bought = 70;
input over_Sold = 70;

input averageType = AverageType.WILDERS;


def NetChgAvg = MovingAverage(averageType, price - price[1], rsi_length);
def TotChgAvg = MovingAverage(averageType, AbsValue(price - price[1]), rsi_length);
def ChgRatio = if TotChgAvg != 0 then NetChgAvg / TotChgAvg else 0;

def RSI = 50 * (ChgRatio + 1);

plot SMA_tip = (price > SMA AND (price[1] < SMA[1])
and SMA > SMA[20]) and RSI >= over_Bought;

#plot UpSignal = price crosses above SMA;
#plot DownSignal = price crosses below SMA;

#UpSignal.SetHiding(!showBreakoutSignals);
#DownSignal.SetHiding(!showBreakoutSignals);

#SMA.SetDefaultColor(GetColor(1));
#UpSignal.SetDefaultColor(Color.UPTICK);
#UpSignal.SetPaintingStrategy(PaintingStrategy.BOOLEAN_ARROW_UP);
#DownSignal.SetDefaultColor(Color.DOWNTICK);
#DownSignal.SetPaintingStrategy(PaintingStrategy.BOOLEAN_ARROW_DOWN);