declare lower;

def r1 = RSI();

plot sig1 = if 
    #(r1 crosses above 30 OR r1 crosses above 40 ) then -1
    (r1 crosses above 30 ) then -1
    else if r1 crosses below 70 
    then 1 else 0;