#plot Data = close;
declare lower;
input len = 50;
def PETD = HullMovingAvg(close, len);

plot trend = close > PETD
AND RSI() > 50
;

AssignPriceColor(if trend then Color.GREEN else Color.RED);