declare lower;

def upSwing = low[1] < low[2] and low[1] < low;

#plot downSwing = high[1] > high[2] and high[1] > high;
def exitLength = 20;
def entryPrice = upswing;
#def afterEntryCount = if IsNaN(entryPrice[1]) and !IsNaN(entryPrice) then 1 else if !IsNaN(entryPrice) then afterEntryCount[1] + 1 else Double.NaN;

def afterEntryCount = if entryPrice then afterEntryCount[1] + 1 else 1  ;

plot sell =  afterEntryCount > exitLength;