#plot Data = close;
declare lower;
input len = 20;
def PETD = ExpAverage(close, len);

plot trend = close > PETD;

AssignPriceColor(if close > PETD then Color.GREEN else Color.RED);