declare lower;

input sma1Len = 200;
input ema1Len = 600;

def sma1 = MovingAverage(data = CLOSE, length = sma1Len);
def ema1 = MovingAverage(data = CLOSE, length = ema1Len, averageType = AverageType.EXPONENTIAL);

plot sig1 =
#stay on trend 
#close > sma1 
#AND close > ema1
#AND sma1 > ema1

close > ema1

;