declare lower;
input price = close;
input ema1Len = 8;
input ema2Len = 50;
#input ema3Len = 200;
input colorTrend = 1;

def e1 = MovingAverage(averageType = AverageType.EXPONENTIAL, price, ema1Len);
def e2 = MovingAverage(averageType = AverageType.EXPONENTIAL, price, ema2Len);
#def e3 = MovingAverage(averageType = AverageType.EXPONENTIAL, price, ema3Len);

#plot sig1 = price > e1 AND e1 > e2;

# e1 AND e1 > e2

def smi = StochasticMomentumIndex(40, -40,3, 5);

def stSignal = smi > smi[1] AND smi < smi[6] AND smi < 40 AND smi[6] > 40;

plot sig1 = price > price[1] AND price > e2 AND price[1] < e2
and stSignal
;

#AssignPriceColor(if sig1 then Color.GREEN else Color.GRAY);