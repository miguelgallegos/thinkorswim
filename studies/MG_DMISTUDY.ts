declare lower;
# DMI
def ADX = DMI().ADX;
def DPlus = DMI();
def DMinus = DMI()."DI-";
def Div = 20;

def diff = (DPlus - DMinus);
def sig1 = 
    ADX > Div 
    AND DPlus > DMinus
    AND DPlus > Div
    #AND diff > 3
;

# Parabolic SAR
plot psar = ParabolicSAR();


def c1 = MovingAvgCrossover(length1 = 9, length2 = 20, "average type1" = "EXPONENTIAL", "average type2" = "EXPONENTIAL");
def c2 = MovingAvgCrossover(length1 = 20, length2 = 50, "average type1" = "EXPONENTIAL", "average type2" = "EXPONENTIAL");

def maCrosses = c1 AND c2;