declare lower;

def ma0 = MovingAverage(data = close, length = 9);
def ma1 = MovingAverage(data = close, length = 20);
def ma2 = MovingAverage(data = close, length = 40);
def maX = ma1 > ma2;

def psar = ParabolicSAR();
def psarBelow = psar <= low;

def adx1 = ADX();

def rsi1 = RSI();

#def belowDistance = 
plot sig1 = 
#maX 
#AND 
psarBelow 
AND adx1 > 20 
#AND rsi1 > 50
AND close > ma0 #fastest MA = 9
;
AssignPriceColor(if sig1 then Color.GREEN else Color.RED);
