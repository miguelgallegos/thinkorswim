declare lower;
#plot sa1 = TTM_ScalperAlert();

def sa2 = TTM_ScalperAlert().PivotLow;

plot scalperAlertIsDown = sa2 == 1;