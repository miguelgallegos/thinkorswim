#declare lower;
input price = close;
input length = 9;
input lengthSlow = 20;
input displace = 0;
input checkBackDays = 3;
input showBreakoutSignals = no;

def AvgExpFast = ExpAverage(price[-displace], length);
def AvgExpSlow = ExpAverage(price[-displace], lengthSlow);

#plot diff1 = ((AvgExpFast - AvgExpSlow) / AvgExpSlow) * 100;

#plot diff2 = ((AvgExpFast[checkBackDays] - AvgExpSlow[checkBackDays]) / AvgExpSlow[checkBackDays]) * 100;

#plot res1 = diff2 - diff1;

#def height = AvgExpSlow - AvgExpSlow[1];

#plot “Angle, deg” = Atan(height/lengthSlow) * 180 / Double.Pi;

#decimal for angle values
#“Angle, deg”.AssignValueColor (if “Angle, deg” >= .50 then color.CYAN else color.ORANGE);

AssignPriceColor(if price > AvgExpFast AND AvgExpFast > AvgExpSlow then Color.CYAN else Color.PINK);