#
# MG
#

declare lower;

input sma1 = 50;
def dailyClose = close;
#plot weeklyOpen = close(period = AggregationPeriod.WEEK);
def weeklyOpen = open(period = AggregationPeriod.WEEK);
def weeklyClose = close(period = AggregationPeriod.WEEK);

#plot monthlyOpen = close(period = AggregationPeriod.MONTH);
#plot monthlyClose = close(period = AggregationPeriod.MONTH);
def monthlyOpen = open(period = AggregationPeriod.MONTH);
def monthlyClose = close(period = AggregationPeriod.MONTH);

def SMA = Average(dailyClose, sma1);

def signal1 = 
    ((monthlyClose > monthlyClose[1] and monthlyClose[1] > monthlyOpen[1]) 
and 
    (weeklyClose > weeklyClose[1] and weeklyClose[1] > weeklyOpen[1]))
#and (dailyClose > SMA)
;

def signal2 = dailyClose > SMA;

plot periodsCheck = signal1;
plot aboveSma = signal2;
#plot signal3 = signal1 and signal2 ;

#signal1.SetDefaultColor(Color.DARK_GREEN);
#signal2.SetDefaultColor(Color.DARK_ORANGE);
#signal3.SetDefaultColor(Color.ORANGE);
#plot mc1 = monthlyClose;

