input price = close;
input lengthTrend = 50;
def smaTrend = MovingAverage(data = CLOSE, length = lengthTrend);

def smaSignal = MovingAverage(data = CLOSE, length = 20);
#smaTrend.SetDefaultColor(GetColor(10));

AssignPriceColor(
if
price > smaTrend # AND
#smaSignal > smaTrend
then Color.GREEN else Color.GRAY
);

#ORIGINAL -- 
#def ps = ParabolicSAR();
#def ad = ADX();
#AssignPriceColor(if price > smaTrend
#AND price > smaSignal
#AND price > ps
#AND ad > 18
# then Color.GREEN else Color.GRAY);