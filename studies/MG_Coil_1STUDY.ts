declare lower;

def ma20 = MovingAverage(data = close, length = 20);
def ma50 = MovingAverage(data = close, length = 50);
def ma60 = MovingAverage(data = close, length = 60);
def ma60V = MovingAverage(data = volume, length = 60);

plot signal1 = 
    ma60V > 500000
    and ma60 > 10
    and close >= open
    and high <= high[5]
    and low >= low[5]
    and ma20 > ma50 * 1.08;


def ADX = DMI().ADX;

plot signal2 = 
    ma60V > 500000
    and ma60 > 10
    and ma50 < ma20
    and ADX > ADX[20] * 0.41;

