declare lower;
input obvSlowLength = 50;
input obvFastLength = 7;
def o1 = OnBalanceVolume();

def omaSlow = MovingAverage(data = o1, length = obvSlowLength, averageType = AverageType.WEIGHTED);

def omaFast = MovingAverage(data = o1, length = obvFastLength, averageType = AverageType.WEIGHTED);

#def ma = MovingAverage(length = 50, data = CLOSE);
def ma = TEMA(length = 50);

def upSlow = omaSlow > omaSlow[1];
def upFast = omaFast > omaFast[1];
def upMa = ma > ma[1];

plot sig = upSlow + upFast
+ upMa
;

sig.AssignValueColor(
    if sig == 3 then Color.GREEN 
      else if sig == 2 then Color.ORANGE 
        else if sig == 1 then Color.YELLOW else Color.RED
);