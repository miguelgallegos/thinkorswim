declare lower;

input emaLen = 60;
input cciLen = 14;

def ema = MovingAverage(data = close, averageType = AverageType.EXPONENTIAL, length = emaLen);

def ad = ADX();

def ci = CCI();

plot sig1 = close >= ema 
AND ci >= 100
AND ad >= 20
;