declare lower; 

def IsUp = close > open;
def IsDown = close < open;
def IsDoji = IsDoji();
def IsLongWhite = IsLongWhite();
def avgRange = 0.05 * Average(high - low, 20);
def revEngRSI = reverseEngineeringRSI();
def range = close - open;

plot PatternPlot = 

# 2.- BASIC - strict
# must pass always
IsUp[0] AND
IsUp[2] AND
high[1] <= ( close[0] - ( range[0] / 4) ) AND
low[2] < low[1] AND
close > revengRSI AND
( ( AbsValue( range[1] ) * 100 ) / range[0] ) < 50 AND
(
    ( ( ( AbsValue( range[1] ) * 100 ) / range[2] ) < 50 ) #OR
   #( ( ( AbsValue( range[1] ) * 100 ) / range[3] ) < 20 )
)


#(  AbsValue( range[1] ) * 100 ) / range[2] ) < 20 OR
#    AbsValue( range[1] ) * 100 ) / range[3] ) < 20 ) 

# 1.-BASIC -- visual 
#AbsValue(range[1] * 4) < (range[0]) AND
#AbsValue(range[1]) < range[0] AND 
#AbsValue(range[1]) < (range[2] AND range[3]) AND
#IsUp[0] AND (IsUp[2] OR IsUp[3])
#AND high[1] < (close + open) / 2
#basic


#   AbsValue(range[1] * 5) >= (range[0] * .9 ) AND
#AbsValue(range[1] * 4) < (range[0] * 1.2) AND

#   AbsValue(range[1] * 5) >= (range[2] * .9) AND
#AbsValue(range[1] * 4) < (range[2] * 1.2) AND

### height of 0 and 2
   #AbsValue(range[1] * 5) <= (range[0] * .9 ) AND
   #AbsValue(range[1] * 5) <= (range[2] * .9 ) AND

### SO SO
#AbsValue(range[1]) < (range[0]) AND
#AbsValue(range[1]) < (range[2]) AND
#   IsUp[2] and
#    IsUp[0] 
#    AND IsLongWhite[0] AND IsLongWhite[2]
#    AND high[1] < high[2]
#    AND low[1] > low[2]


    #AND
    #close[1] >= (close[2] - (close[2] / 4)) #limit how far the close goes up
    #AND
    #open[1] >= (open[0] - (open[0] / 4))

    #AND (range[0] - range[2]) < .05



#    close[2] <= open[1] and
#    close[1] <= open[0]  

;

#PatternPlot.SetPaintingStrategy(PaintingStrategy.BOOLEAN_ARROW_UP);
#PatternPlot.SetDefaultColor(GetColor(0));