declare lower;

def macdVal = MACD();
def macdAvg = MACD().Avg;

def macdUp = macdVal > macdAvg;

def ao = AwesomeOscillator();

#def mfi1 = MoneyFlowIndex();

def t1 = TEMA(length = 50);

def tx1 = TRIX();

plot macdAO = (macdUp AND ao > 0) * 1;

#plot macdAO = (macdUp AND mfi1 > 0) * 1;
plot macdTema = (macdUp AND close > t1) * 2;

plot macdTrix = (macdUp AND tx1 > 0) * 3;