#average(volume,50) > 500000;
declare lower;
#plot vol = volume;
#vol.SetPaintingStrategy(PaintingStrategy.HISTOGRAM);
def volAverageFaster = Average(volume,5);
def volAverageFast = Average(volume,14);
def volAverage = Average(volume,50); # was 50

plot volumeUp = volAverageFaster > volAverageFaster[5]
AND volAverageFaster > volAverageFast 
#AND volAverageFast > volAverage ## too extreme...
AND volume > (volume[1] * 1.20 ) 
AND volume > 20000
AND close > close[1] * 1.02
;

#plot posVolX = PositiveVolumeIndex();
#plot negVolX = NegativeVolumeIndex();