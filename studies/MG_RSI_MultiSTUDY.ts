declare lower;
input len1 = 6;
input len2 = 14;
input len3 = 24;

plot rsisBuy = RSI(length = len1) > RSI(length = len2)
AND RSI(length = len2) > RSI(length = len3);