#
# TD Ameritrade IP Company, Inc. (c) 2007-2018
#

declare lower;

input trix_length = 9;
input trix_colorNormLength = 14;
input trix_price = close;
input signalLength = 3;

input smaAbove = 50;

def smaAboveCheck = MovingAverage(data = trix_price, length = smaAbove);

def tr = ExpAverage(ExpAverage(ExpAverage(Log(trix_price), trix_length), trix_length), trix_length);

def TRIX = (tr - tr[1]) * 10000;
def Signal = ExpAverage(TRIX, signalLength);
#plot ZeroLine = 0;

#TRIX.DefineColor("Highest", Color.YELLOW);
#TRIX.DefineColor("Lowest", Color.LIGHT_RED);
#TRIX.AssignNormGradientColor(trix_colorNormLength, #TRIX.Color("Lowest"), TRIX.Color("Highest"));

#Signal.SetDefaultColor(GetColor(3));
#ZeroLine.SetDefaultColor(GetColor(5));

# CCI 

input cci_length = 14;
input over_sold = -100;
input over_bought = 100;
input showBreakoutSignals = no;

def price = close + low + high;
def linDev = LinDev(price, cci_length);
def CCI = if linDev == 0 then 0 else (price - Average(price, cci_length)) / linDev / 0.015;


plot TRIX_CCI = (
    TRIX > Signal and 
    CCI > over_sold and 
    CCI[1] < CCI and 
    CCI < over_bought)
        AND smaAboveCheck > smaAboveCheck[3]
        AND trix_price > smaAboveCheck
 #       AND trix_price > (smaAboveCheck * 1.03)
;
 
#plot tprice = trix_price;
#plot sprice = smaAboveCheck;
# * -1 + (TRIX < Signal)

#plot OverBought = over_bought;
#plot ZeroLine = 0;
#plot OverSold = over_sold;

#plot UpSignal = if CCI crosses above ZeroLine then ZeroLine else Double.NaN;

#plot DownSignal = if CCI crosses below ZeroLine then ZeroLine else Double.NaN;

#UpSignal.SetHiding(!showBreakoutSignals);
#DownSignal.SetHiding(!showBreakoutSignals);

#CCI.SetDefaultColor(GetColor(9));
#OverBought.SetDefaultColor(GetColor(5));
#ZeroLine.SetDefaultColor(GetColor(5));
#OverSold.SetDefaultColor(GetColor(5));
#UpSignal.SetDefaultColor(Color.UPTICK);
#UpSignal.SetPaintingStrategy(PaintingStrategy.ARROW_UP);
#DownSignal.SetDefaultColor(Color.DOWNTICK);
#DownSignal.SetPaintingStrategy(PaintingStrategy.ARROW_DOWN);