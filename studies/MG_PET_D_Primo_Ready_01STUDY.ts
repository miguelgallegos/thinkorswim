#plot Data = close;
declare lower;
def PETD = ExpAverage(close, 15);

plot go1 = close > PETD
    AND close[1] > PETD[1]
    AND close[2] < PETD[2];