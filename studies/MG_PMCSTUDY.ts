declare lower;
plot pmcDiff = PMC();
plot pmcDiffMA = PMC().DiffMA;

pmcDiff.setPaintingStrategy(PaintingStrategy.HISTOGRAM);
pmcDiff.AssignValueColor(
  if pmcDiff > 0 AND (pmcDiffMA > pmcDiffMA[1]) then Color.CYAN else
    
     if pmcDiff > 0 AND (pmcDiffMA < pmcDiffMA[1]) then Color.BLUE else 
        if pmcDiff < 0 AND (pmcDiffMA < pmcDiffMA[1]) then Color.RED else 
            Color.ORANGE

);

pmcDiffMA.AssignValueColor(
    if pmcDiffMA > pmcDiffMA[1] then Color.YELLOW else Color.LIGHT_RED

);
pmcDiffMA.setLineWeight(2);
## need color on DiffMA line too

plot zl = 0;