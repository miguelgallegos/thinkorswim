# double cci strategy for trend following
 
declare lower;

input cciLength1 = 20;
input cciLength2 = 100;

def c1 = CCI(length = cciLength1);
def c2 = CCI(length = cciLength2); 

#plot sig1 = c2 > -100;
plot sig1 = 
#c2 crosses above 0 AND
#c1 crosses above 0

c2 > 90 AND
c1 > 90

#c2 > -100 AND
#c1 > 0
#c2 crosses above 80
#AND c1 > 80
;

