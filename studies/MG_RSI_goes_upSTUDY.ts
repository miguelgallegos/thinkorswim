declare lower;
def ma = MovingAverage(length = 50, data = CLOSE);
def r1 = RSI();
def c1 = CCI(length = 50);
def ax1 = ADX();

plot rsiGoesUp = r1 > 50 
#AND close > ma
#AND ma > ma[1]
#AND ax1 > 25
#AND c1 > 0
;