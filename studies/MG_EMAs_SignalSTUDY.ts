declare lower;
###
# BUY bar closestPercent near to tren MA (slower)
# Stay if all GREEN or if retracements to fast MA occurs
# SELL ??
##
# include RSI or another indicator to support BUY signal
input price = close;
input ema1Len = 8;
input ema2Len = 50;
input ema3Len = 200;
input colorTrend = 1;
input closestPercent = 1.5;

def e1 = MovingAverage(averageType = AverageType.EXPONENTIAL, price, ema1Len);
def e2 = MovingAverage(averageType = AverageType.EXPONENTIAL, price, ema2Len);
def e3 = MovingAverage(averageType = AverageType.EXPONENTIAL, price, ema3Len);

#plot sig1 = e1 > e2 AND e2 > e3;
def sig1 = price > e1 AND e1 > e2;

#how much above the trend EMA
def percentAboveEma2 = ((e2 - price) / e2) * 100;

#MY MG INDICATOR !!!
plot valPercent2 = percentAboveEma2 * -1;

plot buy = sig1 AND AbsValue(percentAboveEma2) < closestPercent;

#valPercent2.SetPaintingStrategy(paintingStrategy = PaintingStrategy.HISTOGRAM);

#valPercent2.AssignValueColor(if sig1 then Color.GREEN else Color.GRAY);
#valPercent2.AssignValueColor(if buy then Color.CYAN else Color.CURRENT);

AssignPriceColor(if sig1 then Color.GREEN else Color.GRAY);
AssignPriceColor(if buy then Color.CYAN else Color.CURRENT);
