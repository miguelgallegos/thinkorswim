#
# TD Ameritrade IP Company, Inc. (c) 2007-2018
#

declare lower;

input rsi_length = 5;
input rsi_over_Bought = 50;
input rsi_over_Sold = 50;
input price = close;
input averageType = AverageType.WILDERS;
input showBreakoutSignals = no;

def NetChgAvg = MovingAverage(averageType, price - price[1], rsi_length);
def TotChgAvg = MovingAverage(averageType, AbsValue(price - price[1]), rsi_length);
def ChgRatio = if TotChgAvg != 0 then NetChgAvg / TotChgAvg else 0;

def RSI = 50 * (ChgRatio + 1);
#plot OverSold = rsi_over_Sold;
#plot OverBought = rsi_over_Bought;
#plot UpSignal = if RSI crosses above OverSold then OverSold else Double.NaN;
#plot DownSignal = if RSI crosses below OverBought then OverBought else Double.NaN;

#UpSignal.SetHiding(!showBreakoutSignals);
#DownSignal.SetHiding(!showBreakoutSignals);

#RSI.DefineColor("OverBought", GetColor(5));
#RSI.DefineColor("Normal", GetColor(7));
#RSI.DefineColor("OverSold", GetColor(1));
#RSI.AssignValueColor(if RSI > over_Bought then #RSI.color("OverBought") else if RSI < over_Sold then #RSI.color("OverSold") else RSI.color("Normal"));
#OverSold.SetDefaultColor(GetColor(8));
#OverBought.SetDefaultColor(GetColor(8));
#UpSignal.SetDefaultColor(Color.UPTICK);
#UpSignal.SetPaintingStrategy(PaintingStrategy.ARROW_UP);
#DownSignal.SetDefaultColor(Color.DOWNTICK);
#DownSignal.SetPaintingStrategy(PaintingStrategy.ARROW_DOWN);

#SMA
#input price = close;
input sma_length = 50;
input displace = 0;
#input showBreakoutSignals = no;

def SMA = Average(price[-displace], sma_length);


plot RSI_SMA_Crossover = SMA < price and
RSI > rsi_over_Bought;


#plot UpSignal = price crosses above SMA;
#plot DownSignal = price crosses below SMA;

#UpSignal.SetHiding(!showBreakoutSignals);
#DownSignal.SetHiding(!showBreakoutSignals);

#SMA.SetDefaultColor(GetColor(1));
#UpSignal.SetDefaultColor(Color.UPTICK);
#UpSignal.SetPaintingStrategy(PaintingStrategy.BOOLEAN_ARROW_UP);
#DownSignal.SetDefaultColor(Color.DOWNTICK);
#DownSignal.SetPaintingStrategy(PaintingStrategy.BOOLEAN_ARROW_DOWN);