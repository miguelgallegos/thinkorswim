input vwmaLength = 50;

plot VWMA = Sum(volume * close, vwmaLength) / Sum(volume, vwmaLength);