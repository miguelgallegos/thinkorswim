declare lower;
input cciLen = 14;
def ci = CCI(length = cciLen);
def ad = ADX();
def rs = RSI();
plot sig = if ci crosses above -100 and ad >= 20 and rs >= 40 then -1 
else if 
ci > 100 and ci[1] > 100 #and ci[2] < 100 
#AND ci[1] > ci[2] 
then 1 else 0;
